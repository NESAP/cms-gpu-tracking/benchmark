#!/bin/bash

#SBATCH -C gpu
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --exclusive
#SBATCH -t 02:00:00
#SBATCH --image=docker:makortel/cmssw_patatrack:11_0_0_pre7_v1
#SBATCH --module=cvmfs,gpu
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=matti@fnal.gov

MAXSTREAMS=16
NGPU=1

srun -n 1 -c 16 shifter /global/homes/m/matti/nesap/benchmark/cori_gpu/runProfile.sh $MAXSTREAMS $NGPU
