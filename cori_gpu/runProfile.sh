#/bin/bash

PREFIX="/global/cscratch1/sd/matti/patatrack_data"
MAXSTREAMS=${1}
NGPU=${2}

function run()
{
    local NTH=${1}
    local CFG=${2}
    local POSTFIX=${3}
    local MAX=${4}

    for (( ITER=1; ITER<=${MAX}; ++ITER)); do
	#echo "/usr/bin/time cmsRun -n ${NTH} ${CFG} > out_cori_gpu${NGPU}_${POSTFIX}_${ITER}.txt"
	/usr/bin/time cmsRun -n ${NTH} ${CFG} > out_cori_gpu${NGPU}_${POSTFIX}_${ITER}.txt 2>&1
    done
}

source /opt/cms/cmsset_default.sh
cd $SCRATCH/CMSSW_11_0_0_pre7_Patatrack/src
cmsenv

run 8 readFromPixelRaw.py read 8
run 8 profile_daq.py warmup 4

for (( ISTREAM=1; ISTREAM <= ${MAXSTREAMS}; ++ISTREAM)); do
    run ${ISTREAM} profile_daq.py cores${ISTREAM} 8
#    run ${ISTREAM} profile_daq_transfer.py transfer_cores${ISTREAM} 8
#    run ${ISTREAM} profile_daq_realistic_triplet.py realistic_triplet_cores${ISTREAM} 8
done
