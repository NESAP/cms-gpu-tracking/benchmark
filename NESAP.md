# CMS GPU tracking

## Table of Contents

* [Sub-pages](#sub-pages)
* [Description](#description)
* [Figure of Merit](#figure-of-merit)
* [Measurement on Edison (10_5_0_pre2 version only)](#measurement-on-edison-10_5_0_pre2-version-only)
  * [Results](#results)
* [Measurement on Cori Haswell](measurement-on-cori-haswell)
  * [Results](#results-1)
* [Measurements on Cori GPU (Skylake, CPU only)](#measurements-on-cori-gpu-skylake-cpu-only)
  * [Results](#results-2)
* [Measurements on Cori GPU](#measurements-on-cori-gpu)
  * [Results](#results-3)
  * [Recipe](#recipe)
    * [One-time setup](#one-time-setup)
    * [Checking out packages and building the code](#checking-out-packages-and-building-the-code)
    * [Run the application interactively](#run-the-application-interactively)
    * [Summary of configuration files](#summary-of-configuration-files)
    * [Using `nvprof`](#using-nvprof)
    * [Run the benchmarks through a batch job](#run-the-benchmarks-through-a-batch-job)

## Sub-pages

* [Changelog of CMSSW Patatrack releases](Changelog.md)
* [More detailed description of CMSSW and the application](CMSSW.md)
* [More detailed results on performance measurements on Cori GPU nodes](Cori_GPU.md)

## Description

For more details please see the first reference. The NESAP benchmark
problem is a part of the CMS full event reconstruction running on CPUs
that has been re-engineered for GPUs. The application runs the
reconstruction chain from raw pixel detector data up to pixel tracks
and vertices. For more details, see [CMSSW.md](CMSSW.md).

The benchmark job processes in total 4200 events. The job prints
timestamps at the beginning of each 100 events. The throughput is
extracted as the time to process the events from 101st to 4101st, i.e.
the central 4000 events of the total set. This way the job startup
time is neglected, which reflects the production jobs that run for
several hours.

References:

* https://cms-docdb.cern.ch/cgi-bin/PublicDocDB/ShowDocument?docid=13767
* https://patatrack.web.cern.ch/patatrack/wiki/
* https://github.com/cms-patatrack/cmssw

The application is in a shifter container image (see instructions
below) that has been downloaded from the dockerhub image
[makortel/cmssw_patatrack:11_0_0_pre7_v1](https://hub.docker.com/layers/makortel/cmssw_patatrack/11_0_0_pre7_v1/images/sha256-8fce0d58da072c532276b434115a20c584a23222eae870162015f04415d4684b).

## Figure of Merit

The Figure of Merit is throughput expressed as number of processed
events / second. 

## Measurement on Edison (10_5_0_pre2 version only)

The measurement on Edison is done with a single node. Each node has
two sockets, and each socket is popuplated with a 12-core Intel "Ivy
Bridge" processor, so there are 24 physical cores per node. Each core
has 2 hardware threads, leading to 48 logical cores.

The input data files (order of 1 GB) 

Ask for resource with the docker image
```shell
edison$ salloc -A ... -N 1 --image=docker:makortel/cmssw_patatrack:10_5_0_pre2_v0.11 --module=cvmfs
```

One-time setup (`.../benchmark` refers to a clone of this benchmark reposito)
```shell
edison$ shifter /bin/bash
edison (container)$ source /opt/cms/cmsset_default.sh
edison (container)$ cmsrel CMSSW_10_5_0_pre2_Patatrack_CUDA_10_0
edison (container)$ cp .../benchmark/10_5_0_pre2/profile_daq.py .../benchmark/10_5_0_pre2/sourceFromPixelRaw_cff.py CMSSW_10_5_0_pre2_Patatrack_CUDA_10_0/src
```

Run the test (the number of threads to use needed to be manually adjusted in `profile_daq.py`)
```shell
# in this benchmark directory
edison$ shifter cori_haswell/runProfile.sh
edison$ ./figure-of-merit.py CMSSW_10_5_0_pre2_Patatrack_CUDA_10_0/src/out_edison_cores_1_*.txt
edison$ ./figure-of-merit.py CMSSW_10_5_0_pre2_Patatrack_CUDA_10_0/src/out_edison_cores_12_*.txt
edison$ ./figure-of-merit.py CMSSW_10_5_0_pre2_Patatrack_CUDA_10_0/src/out_edison_cores_24_*.txt
edison$ ./figure-of-merit.py CMSSW_10_5_0_pre2_Patatrack_CUDA_10_0/src/out_edison_cores_48_*.txt
```

### Results

| Cores | Throughput (events/second) |
|-------|----------------------------|
| 1     | 13.514 +- 0.003            |
| 12    | 129.48 +- 0.06             |
| 24    | 237 +- 4                   |
| 48    | 304.0 +- 0.6               |


## Measurement on Cori Haswell

The measurement on Cori Haswell is done with a single node. Each node
has two sockets, and each socket is popuplated with a 16-core Intel
"Haswell" processor, so there are 32 physical cores per node. Each
core has 2 hardware threads, leading to 64 logical cores.

The input data files (order of 1 GB) 

Ask for resource with the docker image
```shell
cori$ salloc -A ... -N 1 --image=docker:makortel/cmssw_patatrack:11_0_0_pre7_v1 --module=cvmfs
```

One-time setup (`.../benchmark` refers to a clone of this benchmark repo)
```shell
cori$ shifter /bin/bash
cori (container)$ source /opt/cms/cmsset_default.sh
cori (container)$ cd $SCRATCH
cori (container)$ cmsrel CMSSW_11_0_0_pre7_Patatrack
cori (container)$ cp .../benchmark/11_0_0_pre7/*.py CMSSW_11_0_0_pre7_Patatrack/src
/src
```

Run the test interactively
```shell
# in this benchmark directory
cori$ shifter cori_haswell/runProfile.sh
```

Run the batch job
```shell
cori$ sbatch -A ... .../benchmark/cori_haswell/submitProfile.sh
```

Collect results
```
cori$ ./figure-of-merit.py CMSSW_11_0_0_pre7_Patatrack/src/out_cori_cores_1_*.txt
cori$ ./figure-of-merit.py CMSSW_11_0_0_pre7_Patatrack/src/out_cori_cores_16_*.txt
cori$ ./figure-of-merit.py CMSSW_11_0_0_pre7_Patatrack/src/out_cori_cores_32_*.txt
cori$ ./figure-of-merit.py CMSSW_11_0_0_pre7_Patatrack/src/out_cori_cores_64_*.txt
```

### Results

| Cores | Throughput (events/second) 10_5_0_pre2 |  11_0_0_pre7  |
|-------|----------------------------------------|---------------|
| 1     | 16.23 +- 0.05                          | 23.2 +- 0.1   |
| 16    | 187.1 +- 0.1                           | 266.0 +- 0.8  |
| 32    | 302 +- 3                               | 404 +- 4      |
| 64    | 404.6 +- 0.7                           | 520 +- 14     |


## Measurements on Cori GPU (Skylake, CPU only)

### Results

| Cores | Throughput (events/second) 10_5_0_pre2 |  11_0_0_pre7  |
|-------|----------------------------------------|---------------|
| 1     | 16.71 +- 0.09                          | 24.52 +- 0.04 |
| 20    | 288.5 +- 0.9                           | 429 +- 2      |
| 40    | 440 +- 10                              | 591 +- 5      |
| 80    | 532 +- 2                               | 846 +- 6      |

## Measurements on Cori GPU

### Results

Peak throughput (for more details see [Cori_GPU.md](Cori_GPU.md))

| GPUs | Throughput (events/second): 10_5_0_pre2 | 11_0_0_pre7 |
|------|-----------------------------------------|-------------|
| 1    | 1883 +- 6                               | 1840 +- 20  |
| 2    | 3400 +- 10				 | 2590 +- 60  |
| 3    | 4220 +- 30				 | 2590 +- 40  |
| 4    | 4660 +- 70				 | 2800 +- 30  |
| 5    | 5120 +- 50				 | 2710 +- 40  |
| 6    | 4800 +- 100				 | 2740 +- 50  |
| 7    | 4900 +- 100				 | 2570 +- 50  |
| 8    | 5000 +- 100                             | 2800 +- 30  |

### Recipe


#### One-time setup

Ask for a resource with the docker image
```shell
cori$ module load esslurm
cori$ salloc -A ... -C gpu -N 1 --gres=gpu:1 --image=docker:makortel/cmssw_patatrack:11_0_0_pre7_v1 --module=cvmfs,gpu -t 30
```

Create a CMSSW "developer area". It is needed to run the
application. In addition, if any code is to be modified, that code is
"checked out" and compiled on the developer area
```shell
# Start the container
cori$ srun -n 1 -c 1 --pty shifter /bin/bash -i
# Source necessary environment
cori (container)$ source /opt/cms/cmsset_default.sh
# Create a CMSSW developer area, e.g. in $SCRATCH
cori (container)$ cd $SCRATCH
cori (container)$ cmsrel CMSSW_11_0_0_pre7_Patatrack
cori (container)$ cd $SCRATCH/CMSSW_11_0_0_pre7_Patatrack/src
```

If you intend to check out code and compile, do the following setup
steps. By default the `git cms-init` sets up also the user github
repository and thus requires `name`, `email`, and `github` (user name)
attributes to be set under `[user]` section in the `.gitconfig`. This
"user setup" can be left out with `--upstream-only` parameter. If you
only intend to run the application as it is in the release, you can
safely skip this step and proceed go "Copy job configuration files...".
```shell
cori (container)$ cmsenv
cori (container)$ git cms-init -x cms-patatrack [--upstream-only]
cori (container)$ git branch CMSSW_11_0_X_Patatrack --track cms-patatrack/CMSSW_11_0_X_Patatrack
```

Copy job configuration files from a clone of this benchmark repo into the developer area
```shell
# Assuming $PWD is .../CMSSW_11_0_0_pre7_Patatrack/src
cori (container)$ cp .../benchmark/11_0_0_pre7/*.py .
```

#### Checking out packages and building the code

The code can be checked out an compiled in the developer area as follows
```shell
# Checkout code of some package, e.g. HeterogeneousCore/CUDACore
cori (container)$ git cms-addpkg HeterogeneousCore/CUDACore
# If you edit any header file, check out any other packages depending on that header
cori (container)$ git cms-checkdeps -a
# Compile with N threads
cori (container)$ scram b -j <N>
```
Note that the code can be compiled on a CPU-only node as well, i.e. existence of GPU is not required.

Additional flags can be passed to the host compiler (`g++`) with
`USER_CXXFLAGS` environment variable, and to the device compiler
(`nvcc`) with `USER_CUDA_FLAGS` environment variable. At least with
`bash` shell these variables can be set on a single line along
```shell
cori (container)$ USER_CXXFLAGS="<g++ flags>" USER_CUDA_FLAGS="<nvcc flags>" scram b -j <N>
```
These flags can also be set more permanently in per-package `BuildFile.xml` files.


#### Run the application interactively

```shell
cori$ module load esslurm
cori$ salloc -A ... -C gpu -N 1 --gres=gpu:1 --image=docker:makortel/cmssw_patatrack:11_0_0_pre7_v1 --module=cvmfs,gpu -t 30

# Start the container, source environment if not done yet
cori$ srun -n 1 -c 1 --pty shifter /bin/bash -i
cori (container)$ source /opt/cms/cmsset_default.sh
# Go to the 'src' subdirectory of the CMSSW developer area
cori (container)$ cd $SCRATCH/CMSSW_11_0_0_pre7_Patatrack/src
# Source developer-area specific environment
cori (container)$ cmsenv
# Run the application on GPU
cori (container)$ cmsRun profile_daq.py
# Run the application on CPU
cori (container)$ cmsRun profile_daq_cpu.py
```

Note: at the beginning of the job there are error messages along
`problem with creating filedesc for fuwritelock` and `problem with
opening fuwritelock file stream`, these are safe to ignore.

The recipe above uses 1 thread and 1 concurrent event. Those can be
simultaneously set to `N` with `cmsRun -n <N> profile_daq.py`. Those
parameters can also be controlled invidually by editing the
configuration file, and setting
* `process.options.numberOfThreads` for the number of threads, and
* `process.options.numberOfStreams` for the number of concurrent events.

The `-n` parameter for `cmsRun` sets the number of threads, and if
`numberOfStreams` is set to `0`, as many concurrent events are used as
there are threads.

The `profile_daq.py` configuration does not produce any output (it is
somewhat artificial in that sense).

#### Summary of configuration files

All these files are located in the [`11_0_0_pre7`](11_0_0_pre7)
directory in this repository.

| Config file | Description |
|-------------|-------------|
| `profile_daq.py` | Minimal GPU job |
| `profile_daq_transfer.py` | Minimal GPU job, and transfer resulting tracks and vertices back to CPU at the end of each event |
| `profile_daq_validate.py` | GPU job transferring results back to CPU, converting them to legacy format, and producing histograms to validate results |
| `profile_cpu.py` | CPU version of `profile_daq_transfer.py`, i.e. these two configuration files produce the same output products in CPU memory |

##### Validation

Validation needs `profile_daq_validate.py` and `harvest.py`
configuration files from the [`11_0_0_pre7`](11_0_0_pre7) directory.
Assuming those have been copied to the local directory, run
```shell
# produces step3_inDQM.root file
cori (container)$ cmsRun profile_daq_validate.py
# converts step3_inDQM.root file to DQM_V0001_R000321177__Global__CMSSW_X_Y_Z__RECO.root file
cori (container)$ cmsRun harvest.py
```

The second job produces a
`DQM_V0001_R000321177__Global__CMSSW_X_Y_Z__RECO.root` file that has
the histograms. It is probably a good idea to run one first for the
reference and rename the file to e.g `DQM_reference.root`.

After implementing the changes, recompiling, and running again, two
`DQM*.root` files can be compared with `makeDiff.sh` script.
```shell
cori (container)$ .../makeDiff.sh DQM_reference.root DQM_V0001_R000321177__Global__CMSSW_X_Y_Z__RECO.root
```

The script compares a subset of the histograms and produces a
`result.pdf` file with a page per histogram comparison. The plots show
the reference (first argument) as black, and the test (second
argument) as black.


##### Using `nvprof`

The `nvprof` can be run within the container (they
are provided as part of the shifter image)
```shell
cori (container)$ nvprof -o profile.nvvp [nvprof arguments] cmsRun profile_daq.py
```

There is one line in the `profile_daq.py` that is recommended
(although not necessary) to uncomment when running the profile
```python
# Uncomment this line when running nvprof
#process.load('HeterogeneousCore.CUDAServices.NVProfilerService_cfi')
```
The `NVProfilerService` will add CMSSW framework context information
(module name, EDM stream number, EDM context) into the profile as
markers, making it easy to some idea of the application status on the
CPU side.

##### Profiling with `Nsight Compute`

To run the `Nsight Compute` command line interface within the container:
```shell
cori (container)$ nv-nsight-cu-cli --export "baseline" --force-overwrite --target-processes all --kernel-regex-base function --launch-skip-before-match 0 --section ComputeWorkloadAnalysis --section InstructionStats --section LaunchStats --section MemoryWorkloadAnalysis --section MemoryWorkloadAnalysis_Chart --section MemoryWorkloadAnalysis_Tables --section Occupancy --section SchedulerStats --section SourceCounters --section SpeedOfLight --section WarpStateStats --sampling-interval auto --sampling-max-passes 5 --sampling-buffer-size 33554432 --profile-from-start 1 --clock-control base --apply-rules cmsRun profile_daq.py
```
Results are stored as `baseline.nsight-cuprof-report`. 

#### Run the benchmarks through a batch job

Run the batch job
```shell
cori$ module load esslurm
cori$ sbatch -A ... .../benchmark/cori_gpu/submitProfile.sh
```
