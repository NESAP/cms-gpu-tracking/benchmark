# CMSSW and the application structure

## Quick view on CMS data taking

The proton beams of the LHC collide protons at the center of the CMS
detector at 40 MHz. The first-level trigger (L1T) system (implemented
with custom electronics and FPGAs) selects 100 kHz of "most
interesting" of these. These 100 kHz of events are then fed to a
software-based high-level trigger (HLT) that runs on about 30,000 CPU
cores and further selects about 1 kHz of "most interesting" events. It
follows that the HLT has, on the average, about 300 ms time do to the
necessary event reconstruction. In the "offline" data processing the
triggered events are reconstructed with more sophisticated algorithms
and calibrations with about 30-60 seconds per event.

## CMSSW in general

The CMS data processing software, CMSSW, is a rather generic framework
to process independent chunks of data. In CMS these chunks of data
correspond to triggered proton-proton collisions, and are called
events. The events are processed by modules, that can be either
- analyzers (only read objects of the event),
- producers (in addition can produce new objects to the event), or
- filters (in addition, can decide to stop processing of an event).

The modules form a DAG based on their data dependencies. For more
information see e.g.
- CMS Twiki pages
  - The CMS Offline WorkBook [WorkBook](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBook)
  - The CMS Offline SW Guide [SWGuide](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuide)
  - [SWGuideFrameWork](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideFrameWork)

The modules are implemented in C++ (C++17 in general, CUDA code is in
C++14). A job is configured in Python.

The CMSSW framework is multi-threaded using [Threading Building Blocks
(TBB)](https://www.threadingbuildingblocks.org). An integral part of
the multi-threading is a concept of "concurrent event processor" that
we call "a stream" (to disambiguate from CUDA streams, these streams
are called "EDM streams" from now on). An EDM stream processes one
event at a time ("processing" meaning that each module in the DAG is
run on the event in some order respecting the data dependencies). A
job may have multiple EDM streams, in which case the EDM streams
process their events concurrently. Furthermore, modules processing the
same event that are independent in the DAG are also run
concurrently. All this potential concurrency is exposed as tasks to be
run by the TBB task scheduler. We do not make any assumptions on how
TBB runs these tasks in threads (e.g. number of EDM streams and number
of threads may be different). For more information see e.g.
- CMS TWiki [MultithreadedFrameworkDesignDiscussions](https://twiki.cern.ch/twiki/bin/view/CMSPublic/MultithreadedFrameworkDesignDiscussions)
- Papers
  - [C D Jones et al 2014, J. Phys. Conf. Ser. 513 022034](https://iopscience.iop.org/article/10.1088/1742-6596/513/2/022034)
  - [C D Jones et al 2015, J. Phys. Conf. Ser. 664 072026](https://iopscience.iop.org/article/10.1088/1742-6596/664/7/072026)
  - [C D Jones et al 2017, J. Phys. Conf. Ser. 898 042008](https://iopscience.iop.org/article/10.1088/1742-6596/898/4/042008)

The processing time and memory requirements can vary a lot across
events. In addition, the filtering capability may affect which modules
in the DAG can be run.

## Overall view on the use of CUDA

Our overall aims are to avoid blocking synchronization as much as
possible, and keep all processing units (CPU cores, GPUs) as busy as
we can doing useful work. It follows that we try to have all
operations (memory transfers, kernel calls) asynchronous with the use
of CUDA streams, and that we use callback functions
(`cudaStreamAddCallback()`) to notify the CMSSW framework when the
asynchronous work has finished.

We use a "caching allocator" (based on the one from `cub` library) for
both device and pinned host memory allocations. This approach allows
us to amortize the cost of the `cudaMalloc()`/`cudaFree()` etc, while
being able to easily re-use device/pinned host memory regions for
temporary workspaces, to avoid "conservative" overallocation of
memory, and to avoid constraining the scheduling of modules to
multiple devices.

We use one CUDA stream for each EDM stream ("concurrent event") and
each linear chain of GPU modules that pass data from one to the other
in the device memory. In case of branches in the DAG of modules,
additional CUDA streams are used since there is sub-event concurrency
in the DAG that we want to expose to CUDA runtime.

For more information see [`HeterogeneousCore/CUDACore/README.md`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/HeterogeneousCore/CUDACore/README.md).

## Application structure

The pixel tracking GPU prototype consists of six modules that are run in the following sequence (see more details below)
- `offlineBeamSpot`
- `offlineBeamSpotCUDA`
- `siPixelClustersCUDAPreSplitting`
- `siPixelRecHitsCUDAPreSplitting`
- `caHitNtupletCUDA`
- `pixelVertexCUDA`

The job reads uncompressed raw data for just the pixel detector (about
250 kB/event). This configuration is somewhat artificial, e.g. almost
nothing is transferred back to CPU, at the moment there are no modules
that would consume the data in the format produced by this workflow,
and in offline data processing the input data is
compressed. Nevertheless, this setup is an excellent test bed for
prototyping the GPU application as its throughput is very sensitive to
performance issues.

The BeamSpot transfer (`offlineBeamSpot`, `offlineBeamSpotCUDA`) is
independent of `siPixelClustersCUDAPreSplitting`. 

For more information on the application see
* https://cms-docdb.cern.ch/cgi-bin/PublicDocDB/ShowDocument?docid=13767
* https://patatrack.web.cern.ch/patatrack/wiki/
* [ACAT 2019: A. Bocci: Towards a heterogeneous High Level Trigger farm for CMS](https://indico.cern.ch/event/708041/contributions/3276337/attachments/1810853/2957376/Towards_a_heterogeneous_computing_farm_for_the_CMS_High_Level_Trigger.pdf)
* [Connecting The Dots 2019: F. Pantaleo: Patatrack: accelerated Pixel Track reconstruction in CMS](https://indico.cern.ch/event/742793/contributions/3274390/attachments/1821674/2979847/20190402_Felice_CTD.pdf)
* [F. Pantaleo: New Track Seeding Techniques for the CMS Experiment (PhD thesis)](http://ediss.sub.uni-hamburg.de/volltexte/2018/9070/pdf/Dissertation.pdf)

### BeamSpot transfer (`offlineBeamSpot` and `offlineBeamSpotCUDA`)

These essentially only transfer data from CPU to GPU. The code of the
transferring module (`offlineBeamSpotCUDA`) is in
[`RecoVertex/BeamSpotProducer/plugins/BeamSpotToCUDA.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoVertex/BeamSpotProducer/plugins/BeamSpotToCUDA.cc).

### Raw-to-cluster (`siPixelClustersCUDAPreSplitting`)

The first module in the chain as "raw-to-cluster", that unpacks and
reformats the raw to into something usable to downstream, applies some
calibration, and forms clusters of pixels on each pixel detector module.

The code of the module is in
[`RecoLocalTracker/SiPixelClusterizer/plugins/SiPixelRawToClusterCUDA.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoLocalTracker/SiPixelClusterizer/plugins/SiPixelRawToClusterCUDA.cc)
with the kernels being launched defined in
- [`RecoLocalTracker/SiPixelClusterizer/plugins/SiPixelRawToClusterGPUKernel.cu`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoLocalTracker/SiPixelClusterizer/plugins/SiPixelRawToClusterGPUKernel.cu)
- [`RecoLocalTracker/SiPixelClusterizer/plugins/gpuCalibPixel.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoLocalTracker/gpuCalibPixel.h)
- [`RecoLocalTracker/SiPixelClusterizer/plugins/gpuClusterChargeCut.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoLocalTracker/gpuClusterChargeCut.h)
- [`RecoLocalTracker/SiPixelClusterizer/plugins/gpuClusteringConstants.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoLocalTracker/gpuClusteringConstants.h)
- [`RecoLocalTracker/SiPixelClusterizer/plugins/gpuClustering.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoLocalTracker/gpuClustering.h)

### Rechits (`siPixelRecHitsCUDAPreSplitting`)

Second is "rechits", which provides a 3D position estimate for each cluster. Module is in
[`RecoLocalTracker/SiPixelRecHits/plugins/SiPixelRecHitCUDA.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoLocalTracker/SiPixelRecHits/plugins/SiPixelRecHitCUDA.cc)
with the kernels being launched in
[`RecoLocalTracker/SiPixelRecHits/plugins/PixelRecHits.cu`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoLocalTracker/SiPixelRecHits/plugins/PixelRecHits.cu)
and kernels defined in
[`RecoLocalTracker/SiPixelRecHits/plugins/gpuPixelRecHits.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoLocalTracker/SiPixelRecHits/plugins/gpuPixelRecHits.h)

### Pattern recognition (with Cellular Automaton) (`caHitNtupletCUDA`)

Third is the "pattern recognition" part (first creating pairs of pixel
hits on adjacent layers, then connecting them to form quadruplets, and
finally fitting). The module is in
[`RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletCUDA.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletCUDA.cc)
with a middle-man helper class in
- [`RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorOnGPU.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorOnGPU.h)
- [`RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorOnGPU.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorOnGPU.cc)

and kernels for pattern recognition part defined in
- [`RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernels.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernels.h)
- [`RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernelsImpl.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernelsImpl.h)
- [`RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernelsAlloc.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernelsAlloc.h)
- [`RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernels.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernels.cc)
- [`RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernels.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/CAHitNtupletGeneratorKernels.cc)
- [`RecoPixelVertexing/PixelTriplets/plugins/gpuPixelDoublets.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/gpuPixelDoublets.h)
- [`RecoPixelVertexing/PixelTriplets/plugins/gpuPixelDoubletsAlgos.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/gpuPixelDoubletsAlgos.h)
- [`RecoPixelVertexing/PixelTriplets/plugins/gpuFishbone.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/gpuFishbone.h)

and kernels for fitting defined in
- [`RecoPixelVertexing/PixelTriplets/plugins/HelixFitOnGPU.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/HelixFitOnGPU.h)
- [`RecoPixelVertexing/PixelTriplets/plugins/HelixFitOnGPU.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/HelixFitOnGPU.cc)
- [`RecoPixelVertexing/PixelTriplets/plugins/BrokenLineFitOnGPU.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/BrokenLineFitOnGPU.h)
- [`RecoPixelVertexing/PixelTriplets/plugins/BrokenLineFitOnGPU.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/BrokenLineFitOnGPU.cc)
- [`RecoPixelVertexing/PixelTriplets/plugins/BrokenLineFitOnGPU.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/BrokenLineFitOnGPU.cc)
- [`RecoPixelVertexing/PixelTriplets/plugins/RiemannFitOnGPU.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/RiemannFitOnGPU.h)
- [`RecoPixelVertexing/PixelTriplets/plugins/RiemannFitOnGPU.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/RiemannFitOnGPU.cc)
- [`RecoPixelVertexing/PixelTriplets/plugins/RiemannFitOnGPU.cu`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelTriplets/plugins/RiemannFitOnGPU.cu)

### Vertexing (`pixelVertexCUDA`)

The last step is vertexing (finding clusters of track "origin points"
that likely correspond to the proton-proton collision points). Module
is in
[`RecoPixelVertexing/PixelVertexFinding/src/PixelVertexProducerCUDA.cc`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelVertexFinding/src/PixelVertexProducerCUDA.cc)
with the kernels defined in
- [`RecoPixelVertexing/PixelVertexFinding/src/gpuVertexFinder.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelVertexFinding/src/gpuVertexFinder.h)
- [`RecoPixelVertexing/PixelVertexFinding/src/gpuVertexFinderImpl.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelVertexFinding/src/gpuVertexFinderImpl.h)
- [`RecoPixelVertexing/PixelVertexFinding/src/gpuClusterTracksByDensity.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelVertexFinding/src/gpuClusterTracksByDensity.h)
- [`RecoPixelVertexing/PixelVertexFinding/src/gpuClusterTracksDBSCAN.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelVertexFinding/src/gpuClusterTracksDBSCAN.h)
- [`RecoPixelVertexing/PixelVertexFinding/src/gpuClusterTracksIterative.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelVertexFinding/src/gpuClusterTracksIterative.h)
- [`RecoPixelVertexing/PixelVertexFinding/src/gpuSplitVertices.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelVertexFinding/src/gpuSplitVertices.h)
- [`RecoPixelVertexing/PixelVertexFinding/src/gpuSortByPt2.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelVertexFinding/src/gpuSortByPt2.h)
- [`RecoPixelVertexing/PixelVertexFinding/src/gpuFitVertices.h`](https://github.com/cms-patatrack/cmssw/blob/CMSSW_11_0_0_pre7_Patatrack/RecoPixelVertexing/PixelVertexFinding/src/gpuFitVertices.h)
