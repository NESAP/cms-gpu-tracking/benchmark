#!/bin/bash

#SBATCH --qos=regular
#SBATCH --nodes=1
#SBATCH -t 04:00:00
#SBATCH --image=docker:makortel/cmssw_patatrack:10_5_0_pre2_v0.11
#SBATCH --module=cvmfs
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=matti@fnal.gov

shifter /global/homes/m/matti/nesap/benchmark/edison/runProfile.sh

