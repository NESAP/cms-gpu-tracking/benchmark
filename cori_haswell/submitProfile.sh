#!/bin/bash

#SBATCH -C haswell
#SBATCH --qos=regular
#SBATCH --nodes=1
#SBATCH -t 04:00:00
#SBATCH --image=docker:makortel/cmssw_patatrack:11_0_0_pre7_v1
#SBATCH --module=cvmfs
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=matti@fnal.gov

shifter /global/homes/m/matti/nesap/benchmark/cori_haswell/runProfile.sh
