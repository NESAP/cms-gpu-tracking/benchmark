# Major changes between benchmarked CMSSW-Patatrack releases

## CMSSW_11_0_0_pre7

- Module re-uses the CUDA stream of its predecessor in the data dependence chain if possible
- "pattern recognition" had lots algorithmic of updates
- BeamSpot is transferred concurrently wrt. "raw-to-cluster"
- Code CPU-only workflow is mostly back-ported from GPU code instead of legacy code

## CMSSW_10_0_0_pre5

First version that was tested
	
