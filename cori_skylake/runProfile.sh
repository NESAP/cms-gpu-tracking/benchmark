#/bin/bash

PREFIX="/global/cscratch1/sd/matti/patatrack_data"
export CUDA_VISIBLE_DEVICES=

function run()
{
    local NTH=${1}
    local POSTFIX=${2}
    local MAX=${3}

    for (( ITER=1; ITER<=${MAX}; ++ITER)); do
	#echo "/usr/bin/time cmsRun -n ${NTH} profile_daq_cpu.py > out_cori_skylake_${POSTFIX}_${ITER}.txt"
	/usr/bin/time cmsRun -n ${NTH} profile_daq_cpu.py > out_cori_skylake_${POSTFIX}_${ITER}.txt 2>&1
    done
}

source /opt/cms/cmsset_default.sh
cd $SCRATCH/CMSSW_11_0_0_pre7_Patatrack/src
cmsenv

run 80 warmup 6

run 1 cores1 4
run 20 cores20 4
run 40 cores40 4
run 80 cores80 4
